# Innoloft Dashboard - Front End Challenge

Author : [Tarun Singh](https://www.tarunsingh.dev/)

## Live Demo

This project is hosted on [Netlify](https://innoloft-challenge.netlify.com): https://innoloft-challenge.netlify.com

## Install & Run Project

Clone the project in a local directory, navigate to it and run -

```
npm install
npm start
```

## Requirements

#### Bonus Points

- [x] styles resemble those of [energieloft.de](https://energieloft.de)
- [x] Seamlessly include in the website.

#### Functionality

- [x] create-react-app
- [x] Basic Tab

  - [x] Change e-mail address
  - [x] Change Password
  - [x] Equality Indicator
  - [x] Accepts Uppercase letters, lowercase letters, numbers and special characters
  - [x] A multi-color password strength indicator - **Custom built**
  - [x] Button to update the user data

- [x] Address Tab

  - [x] Change first name
  - [x] Change Last Name
  - [x] Change address (street, house number, postal code)
  - [x] Change country (Germany, Austria, Switzerland are available)
  - [x] Button to update the user data

- [x] Submit through fake AJAX call.
- [x] Show notification if data is saved or errors if not saved

#### Layout

- [x] CSS from scratch.
- [x] Responsive

# Code structure

The application should at the very least use the following:

- [x] React.js framework
- [x] Styled components
- [x] Componets
  - Header
  - Footer
  - Content
  - Aside
  - Form
    - Basic Data
    - Address

# Deadline

It took me around 18-22 Hours in span of 3 days.

Thank you
