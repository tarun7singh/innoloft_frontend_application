import React, { Component } from "react";

// importing main components
import Header from "./components/Header";
import Footer from "./components/Footer";
import Aside from "./components/Aside";
import Content from "./components/Content";

// Notification Container
import ReactNotifications from "react-notifications-component";

class App extends Component {
  state = {};
  render() {
    return (
      <div>
        <ReactNotifications />
        <Header />
        <Aside />
        <Content />
        <Footer />
      </div>
    );
  }
}

export default App;
