import React from "react";
import styled from "styled-components";
import { useFormik } from "formik";

// react notification stuff and animations
import { store } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import "animate.css";

// Styling for address form tab
const Form = styled.form`
  display: grid;
  grid-template: auto / 1fr 1fr;
  max-width: 300px;
  row-gap: 10px;
  margin: 0 auto;
  padding: 10px;
  justify-items: left;
  * {
    width: 100%;
    border-radius: 2px;
  }
  .update {
    grid-column: 1/3;
  }
  .error {
    color: red;
    justify-self: center;
    grid-column: 1/3;
  }
  .submit {
    width: 80%;
    padding: 4px;
    justify-self: center;
    grid-column: 1/3;
  }
`;

// Validating user inputs
const validate = (values) => {
  const errors = {};
  if (values.firstName !== "" && values.firstName.length < 3) {
    errors.firstName = "First name too short";
  }
  if (values.lastName !== "" && values.lastName.length < 3) {
    errors.lastName = "Last name too short";
  }
  if (values.address !== "" && values.address.length < 10) {
    errors.address = "Address too short";
  }
  return errors;
};

// handling submit event, fake call and notification
const onSubmit = (values, { resetForm }) => {
  fetch("https://httpbin.org/status/200").then((res) => {
    if (res.status === 200) {
      store.addNotification({
        title: "Success",
        message: "Your Data is saved successfully",
        type: "success",
        container: "top-right",
        animationIn: ["animated", "bounceInRight"],
        animationOut: ["animated", "bounceOutRight"],
        dismiss: {
          duration: 5000,
        },
      });
      resetForm({});
    }
  });
};

// set initial values
const initialValues = {
  firstName: "",
  lastName: "",
  address: "",
};

// Address Component
const Address = () => {
  const formik = useFormik({
    initialValues,
    validate,
    onSubmit,
  });
  return (
    <Form onSubmit={formik.handleSubmit}>
      <div className="update">Update Details</div>
      <label htmlFor="firstName">Change First Name</label>
      <input
        id="firstName"
        type="text"
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values.firstName}
      />
      <div className="error">
        {formik.touched.firstName && formik.errors.firstName}
      </div>

      <label htmlFor="lastName">Change Last Name</label>
      <input
        id="lastName"
        type="text"
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values.lastName}
      />
      <div className="error">
        {formik.touched.lastName && formik.errors.lastName}
      </div>

      <label htmlFor="address">Change Address</label>
      <textarea
        id="address"
        type="text"
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values.address}
      />
      <div className="error">
        {formik.touched.address && formik.errors.address}
      </div>

      <label htmlFor="country">Change Country</label>
      <select
        id="country"
        name="country"
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values.color}
      >
        <option value=""></option>
        <option value="germany">Germany</option>
        <option value="austria">Austria</option>
        <option value="switzerland">Switzerland</option>
      </select>

      <div className="error">{formik.errors.form}</div>

      <button type="submit" className="submit">
        Submit
      </button>
    </Form>
  );
};
export default Address;
